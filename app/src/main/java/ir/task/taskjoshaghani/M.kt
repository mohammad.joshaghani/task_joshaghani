package ir.task.taskjoshaghani

import android.util.Log

class M {
    private constructor()
}

fun Any.log() {
    Log.e("Task-Joshaghani", "$this")
}