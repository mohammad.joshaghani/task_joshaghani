package ir.task.taskjoshaghani

import android.app.Application
import android.content.Context
import android.graphics.Point
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import dagger.Binds
import dagger.BindsInstance
import dagger.Component
import dagger.Module
import ir.demo.taskjoshaghani.model.ButtonListModel
import ir.demo.taskjoshaghani.model.ButtonModel
import ir.task.taskjoshaghani.factory.MyViewModelFactory
import ir.task.taskjoshaghani.viewModels.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.horizontal_line.*
import kotlinx.android.synthetic.main.vertical_line.*
import kotlinx.android.synthetic.main.view_button.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {


    @Inject
    lateinit var factory: ViewModelProvider.Factory

    lateinit var viewModel: MainViewModel

    var listButtons = mutableListOf<View>()
    var listDividersVertical = mutableListOf<View>()
    var listDividersHorizontal = mutableListOf<View>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        listButtons = mutableListOf(main_btn1, main_btn2, main_btn3)
        listDividersVertical = mutableListOf(vertical_div1, vertical_div2, vertical_div3)
        listDividersHorizontal = mutableListOf(horizontal_div1, horizontal_div2, horizontal_div3)

        val listButtonsModel = mutableListOf<ButtonModel>()

        var i = 0
        listButtons.forEach {
            i++
            val pos = computePostion(i)
            listButtonsModel.add(ButtonModel(it, pos.first, pos.second))
        }

        val dagger = DaggerMyComponent.builder()
        dagger.setButtonList(ButtonListModel(listButtonsModel))
        dagger.setAplication(application)
        dagger.setContext(this)
        dagger.build().inject(this)

        viewModel = ViewModelProviders.of(this, factory).get(MainViewModel::class.java)

        listButtons.forEach {

            it.setOnTouchListener(object : View.OnTouchListener {
                override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                    return viewModel.onTouch(view, event)
                }

            })

            it.setOnClickListener {
                viewModel.setAlign(it)
            }

        }


        viewModel.alignOrientationLiveData.observe(this) {

            when (it) {
                MainViewModel.AlignOreintation.VERTICAL -> {

                    setVerticalView()

                    Toast.makeText(this, "Oreintation changed to VERTICAL!", Toast.LENGTH_SHORT)
                        .show()
                }
                MainViewModel.AlignOreintation.HORIZONTAL -> {

                    setHorizontalView()
                    Toast.makeText(this, "Oreintation changed to HORIZONTAL!", Toast.LENGTH_SHORT)
                        .show()
                }
            }

        }

        setHorizontalView()

    }

    private fun setHorizontalView() {

        vertical_div.visibility = View.GONE
        horizontal_div.visibility = View.VISIBLE

        var i = 0
        for (model in viewModel.getButtonsModelLiveData().value!!.listViews) {
            model.view.animate().x(model.positionX)
                .setDuration(300)
                .start()
            model.view.animate().y(0f)
                .setDuration(300)
                .start()

            listDividersVertical[i].animate().x(model.positionX)
                .setDuration(50)
                .start()
            listDividersVertical[i].animate().y(0f)
                .setDuration(50)
                .start()

            i++
        }


    }

    private fun setVerticalView() {

        vertical_div.visibility = View.VISIBLE
        horizontal_div.visibility = View.GONE

        var i = 0
        for (model in viewModel.getButtonsModelLiveData().value!!.listViews) {

            model.view.animate().y(model.positionY)
                .setDuration(300)
                .start()
            model.view.animate().x(0f)
                .setDuration(300)
                .start()


            listDividersHorizontal[i].animate().y(model.positionY)
                .setDuration(50)
                .start()

            listDividersHorizontal[i].animate().x(0f)
                .setDuration(50)
                .start()



            i++
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        viewModel.changeOreintation()
        return super.onOptionsItemSelected(item)
    }

    fun computePostion(position: Int): Pair<Float, Float> {
        val display: Display = getWindowManager().getDefaultDisplay()
        val size = Point()
        display.getSize(size)
        val width = size.x
        val height = size.y
        val x = width / 6
        val y = height / 10

        val heightSpan = resources.getDimension(
            R.dimen.height_buttons
        )

        return Pair(
            (((2 * position) - 1) * x) - (heightSpan / 2),
            (((2 * position) + 1) * y) - (heightSpan / 2)
        )
    }

}

@Component(modules = [MyModule::class])
interface MyComponent {
    fun inject(activity: MainActivity)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun setAplication(application: Application)

        @BindsInstance
        fun setContext(content: Context)

        @BindsInstance
        fun setButtonList(model: ButtonListModel)


        fun build(): MyComponent

    }

}


@Module
abstract class MyModule {
    @Binds
    abstract fun bindViewModelFactory(factory: MyViewModelFactory): ViewModelProvider.Factory

}