package ir.task.taskjoshaghani

import android.app.Application
import android.os.Handler

class AppDelegate : Application() {

    companion object {
        lateinit var handler: Handler
    }

    override fun onCreate() {
        super.onCreate()
        handler = Handler()
    }

}