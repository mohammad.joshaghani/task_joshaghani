package ir.task.taskjoshaghani.viewModels

import android.view.MotionEvent
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import ir.demo.taskjoshaghani.model.ButtonListModel
import ir.task.taskjoshaghani.AppDelegate
import ir.task.taskjoshaghani.repository.Repository
import javax.inject.Inject


class MainViewModel @Inject constructor(
    private val repository: Repository,
    private val listButtonModel: ButtonListModel
) : ViewModel() {

    enum class AlignOreintation {
        VERTICAL, HORIZONTAL
    }

    private var dX = 0f
    private var dY = 0f

    private var alignOrientation = AlignOreintation.HORIZONTAL

    private var handleTouch = false

    val alignOrientationLiveData: MutableLiveData<AlignOreintation> by lazy(
        LazyThreadSafetyMode.NONE,
        initializer = { MutableLiveData<AlignOreintation>() })


    @Inject
    fun getButtonsModelLiveData(): MutableLiveData<ButtonListModel> {
        return repository.getMyMutableLiveData()
    }


    fun onTouch(view: View?, event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                dX = view!!.x - event.rawX
                dY = view.y - event.rawY
            }
            MotionEvent.ACTION_MOVE -> {

                when (alignOrientation) {

                    AlignOreintation.VERTICAL ->

                        view!!.animate().x(event.rawX + dX)
                            .setDuration(0)
                            .start()

                    AlignOreintation.HORIZONTAL ->
                        view!!.animate().y(event.rawY + dY)
                            .setDuration(0)
                            .start()
                }
                handleTouch = true
            }

            MotionEvent.ACTION_UP -> {
                if (handleTouch) {
                    AppDelegate.handler.postDelayed({
                        handleTouch = false
                    }, 60)
                }
            }
            else -> return false
        }

        return handleTouch
    }

    fun setAlign(view: View) {
        when (alignOrientation) {
            AlignOreintation.HORIZONTAL -> setAlignHorizontal(view)
            AlignOreintation.VERTICAL -> (setAlignVertical(view))
        }
    }

    private fun setAlignHorizontal(view: View) {
        val dY = view.y
        listButtonModel.listViews.forEach {
            if (it.view != view) {
                it.view.animate().y(dY)
                    .setDuration(1000)
                    .start()
            }
        }
    }

    private fun setAlignVertical(view: View) {
        val dx = view.x
        listButtonModel.listViews.forEach {
            if (it.view != view) {
                it.view.animate().x(dx)
                    .setDuration(1000)
                    .start()
            }
        }
    }

    fun changeOreintation() {
        if (alignOrientation == AlignOreintation.VERTICAL)
            alignOrientation = AlignOreintation.HORIZONTAL
        else alignOrientation = AlignOreintation.VERTICAL
        alignOrientationLiveData.value = alignOrientation
    }


}