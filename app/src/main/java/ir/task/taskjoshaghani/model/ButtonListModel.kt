package ir.demo.taskjoshaghani.model

import android.view.View

data class ButtonListModel(
    var listViews: MutableList<ButtonModel>
)

data class ButtonModel(
    var view: View,
    var positionX: Float,
    var positionY: Float
)