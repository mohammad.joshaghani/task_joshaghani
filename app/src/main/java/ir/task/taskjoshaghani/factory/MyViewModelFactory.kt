package ir.task.taskjoshaghani.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ir.task.taskjoshaghani.viewModels.MainViewModel
import javax.inject.Inject
import javax.inject.Provider

class MyViewModelFactory @Inject constructor(private val myViewModelProvider: Provider<MainViewModel>) :
    ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return myViewModelProvider.get() as T
    }
}