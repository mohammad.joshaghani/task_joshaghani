package ir.task.taskjoshaghani.repository

import androidx.lifecycle.MutableLiveData
import ir.demo.taskjoshaghani.model.ButtonListModel
import javax.inject.Inject

class Repository @Inject constructor() {

    private val mutableLiveData = MutableLiveData<ButtonListModel>()


    @Inject
    fun setMyMutableLiveData(model: ButtonListModel) {
        mutableLiveData.value = model
    }


    fun getMyMutableLiveData(): MutableLiveData<ButtonListModel> {
        return mutableLiveData
    }

}